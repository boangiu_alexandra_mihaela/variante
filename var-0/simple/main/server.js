const express = require("express")
let jsonData=require("./invoice.json")
const app = express()

app.use('/', express.static('public'))

app.get('/data', (req, res) =>{
    res.status(200).send(jsonData);
})

app.listen(8080);

module.exports = app;