import React from 'react'

class Robot extends React.Component {
    
    handleClickButton=(event)=>{
        this.props.onDelete(this.props.item.id);
    }
    render(){
        const {item}=this.props;
        return(
          <div>
          Hi {item.name}
          <input type="button" value="delete" onClick={this.handleClickButton}/>
          </div>  
            );
    }
}

export default Robot;