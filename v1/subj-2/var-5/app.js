const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.locals.products = [
    {
        name: "Iphone XS",
        category: "Smartphone",
        price: 5000
    },
    {
        name: "Samsung Galaxy S10",
        category: "Smartphone",
        price: 3000
    },
    {
        name: "Huawei Mate 20 Pro",
        category: "Smartphone",
        price: 3500
    }
];

app.get('/products', (req, res) => {
    res.status(200).json(app.locals.products);
});

// app.post('/products', (req, res, next) => {
//     res.status(400).json({message: 'Bad request'});
// })

app.post('/products', async(req, res, next) => {
    const product = req.body;
    if(req.body.constructor === Object && Object.keys(req.body).length ===0){
    res.status(500).json({message:"Body is missing"});
    }

    if(!product.name || !product.category || !product.price){
          res.status(500).json({message:"Invlid body format"});
    }
    else if(product.price <0){
           res.status(500).json({message:"Price should be a positive number"});
    }
    let ok=0;
    for(let i=0;i<app.locals.products.length;i++){
        if(product.name === app.locals.products[i].name){
           res.status(500).json({message:"Product already exists"});
           ok=1;
        }
    }
    if(product.name && product.category && product.price > 0 && ok===0){
        app.locals.products.push(product);
           res.status(201).json({message:"Created"});
    }
     res.status(400).json({message: 'Bad request'});
})

module.exports = app;